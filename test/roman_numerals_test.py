import unittest

from app.roman_numerals import convert


class RomanNumeralsTest(unittest.TestCase):
    def test_convert_1(self):
        assert convert(1) == 'I'

    def test_convert_2(self):
        assert convert(2) == 'II'

    def test_convert_3(self):
        assert convert(3) == 'III'

    def test_convert_4(self):
        assert convert(4) == 'IV'

    def test_convert_5(self):
        assert convert(5) == 'V'

    def test_convert_6(self):
        assert convert(6) == 'VI'

    def test_convert_7(self):
        assert convert(7) == 'VII'

    def test_convert_8(self):
        assert convert(8) == 'VIII'

    def test_convert_9(self):
        assert convert(9) == 'IX'

    def test_convert_10(self):
        assert convert(10) == 'X'

    def test_convert_12(self):
        assert convert(12) == 'XII'

    def test_convert_14(self):
        assert convert(14) == 'XIV'

    def test_convert_17(self):
        assert convert(17) == 'XVII'

    def test_convert_19(self):
        assert convert(19) == 'XIX'

    def test_convert_20(self):
        assert convert(20) == 'XX'

    def test_convert_36(self):
        assert convert(36) == 'XXXVI'

    def test_convert_40(self):
        assert convert(40) == 'XL'

    def test_convert_48(self):
        assert convert(48) == 'XLVIII'

    def test_convert_49(self):
        assert convert(49) == 'XLIX'

    def test_convert_50(self):
        assert convert(50) == 'L'

    def test_convert_54(self):
        assert convert(54) == 'LIV'

    def test_convert_57(self):
        assert convert(57) == 'LVII'

    def test_convert_59(self):
        assert convert(59) == 'LIX'

    def test_convert_89(self):
        assert convert(89) == 'LXXXIX'

    def test_convert_90(self):
        assert convert(90) == 'XC'

    def test_convert_94(self):
        assert convert(94) == 'XCIV'

    def test_convert_99(self):
        assert convert(99) == 'XCIX'

    def test_convert_100(self):
        assert convert(100) == 'C'

    def test_convert_101(self):
        assert convert(101) == 'CI'

    def test_convert_300(self):
        assert convert(300) == 'CCC'

    def test_convert_333(self):
        assert convert(333) == 'CCCXXXIII'

    def test_convert_400(self):
        assert convert(400) == 'CD'

    def test_convert_444(self):
        assert convert(444) == 'CDXLIV'

    def test_convert_490(self):
        assert convert(490) == 'CDXC'

    def test_convert_478(self):
        assert convert(478) == 'CDLXXVIII'

    def test_convert_499(self):
        assert convert(499) == 'CDXCIX'

    def test_convert_500(self):
        assert convert(500) == 'D'

    def test_convert_666(self):
        assert convert(666) == 'DCLXVI'

    def test_convert_888(self):
        assert convert(888) == 'DCCCLXXXVIII'

    def test_convert_987(self):
        assert convert(987) == 'DCDLXXXVII'

    def test_convert_999(self):
        assert convert(999) == 'DCDXCIX'

    def test_convert_1000(self):
        assert convert(1000) == 'M'

    def test_convert_1666(self):
        assert convert(1666) == 'MDCLXVI'

    def test_convert_3000(self):
        assert convert(3000) == 'MMM'

    def test_convert_3999(self):
        assert convert(3999) == 'MMMDCDXCIX'
