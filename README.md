# Roman Numerals
The purpose of this coding kata is to write a converter function that takes a number from 1 to 3999 in arabic notation and return its roman numerals notation.

## Install requirements
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```