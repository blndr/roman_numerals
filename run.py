#!/usr/bin/env python3

from app.roman_numerals import convert

print('Roman Numerals converter !')
print('--------------------------')

arabic_numbers = [1, 2, 3, 4, 5, 7, 9, 15, 17, 59, 94, 99, 100, 101, 490, 500, 666, 987, 999, 3000, 3999]

for number in arabic_numbers:
    print(' %d   ->  %s' % (number, convert(number)))
