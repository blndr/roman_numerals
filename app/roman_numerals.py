def convert(number):
    result = 'I' * number
    result = result.replace('IIIII', 'V') \
        .replace('VV', 'X') \
        .replace('XXXXX', 'L') \
        .replace('LL', 'C') \
        .replace('CCCCC', 'D') \
        .replace('DD', 'M') \
        .replace('CCCC', 'CD') \
        .replace('LXXXX', 'XC') \
        .replace('XXXX', 'XL') \
        .replace('VIIII', 'IX') \
        .replace('IIII', 'IV')

    return result
